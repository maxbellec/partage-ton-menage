import React, { useState } from "react";
import "bulma/css/bulma.css";
import "./App.css";
import Task from "./Task";

function App() {
  const [displayForm, setDisplayForm] = useState(false);
  const [taskName, setTaskName] = useState();
  const [taskDuration, setTaskDuration] = useState();
  const [taskFrequency, setTaskFrequency] = useState();
  const [task, setTask] = useState({
    name: "",
    duration: "",
    frequency: "",
  });
  console.log(task);

  const [tasks, setTasks] = useState([]);
  console.log(tasks);

  // const [tasks, setTasks] = useState([
  //     {name: "Passer l'aspirateur", duration: 0.75, frequency: 1},
  //     {name: "Passer la serpillère", duration: 1, frequency: 3},
  //     {name: "Lancer une machine de linge", duration: 0.5, frequency: 1},
  //   ]);

  return (
    <nav
      className="navbar"
      role="navigation"
      aria-label="main navigation"
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <div className="navbar-brand">
        <a
          className="navbar-item"
          href="https://bulma.io"
          style={{ "font-size": "2em" }}
        >
          Partage ton ménage !
        </a>
      </div>
      <div
        className="container"
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <section className="section">
          <button className="button" onClick={() => setDisplayForm(true)}>
            Ajouter une tâche
          </button>
        </section>
        <section>
          <div>
            {displayForm === true ? (
              <form className="form">
                <div className="field">
                  <label className="label">Nom de la tâche</label>
                  <div className="control">
                    <input
                      className="input is-primary"
                      type="text"
                      placeholder="Passer l'aspirateur"
                      value={taskName}
                      onChange={(event) => {
                        setTaskName(event.target.value);
                        setTask({ ...task, name: event.target.value });
                      }}
                    ></input>
                  </div>
                </div>

                <label className="label">Durée (minutes)</label>
                <div className="field has-addons">
                  <div className="control">
                    <input
                      className="input is-primary"
                      type="number"
                      placeholder="60"
                      value={taskDuration}
                      onChange={(event) => {
                        setTaskDuration(event.target.value);
                        setTask({ ...task, duration: event.target.value });
                      }}
                      style={{ width: "70px" }}
                    ></input>
                  </div>
                  <p className="control">
                    <a className="button is-static">minutes</a>
                  </p>
                </div>

                <label className="label">Fréquence</label>
                <div className="field has-addons">
                  <p className="control">
                    <a className="button is-static">toutes les</a>
                  </p>
                  <div className="control">
                    <input
                      className="input is-primary"
                      type="text"
                      placeholder="1"
                      value={taskFrequency}
                      onChange={(event) => {
                        setTaskFrequency(event.target.value);
                        setTask({ ...task, frequency: event.target.value });
                      }}
                      style={{ width: "50px" }}
                    ></input>
                  </div>
                  <p className="control">
                    <a className="button is-static">semaines</a>
                  </p>
                </div>
                <button
                  className="button"
                  type="button"
                  style={{ marginTop: 10 }}
                  onClick={() =>
                    setTasks(Array.prototype.concat(tasks, [task]))
                  }
                >
                  Valider
                </button>
              </form>
            ) : undefined}
          </div>
        </section>

        <section>
          {tasks
            ? tasks.map((task) => (
                <Task
                  name={task.name}
                  duration={task.duration}
                  frequency={task.frequency}
                />
              ))
            : ""}

          {/* <button
            className="button"
            onClick={() => {
              setTasks(
                Array.prototype.concat(tasks, [
                  {
                    name: "Lancer une machine de linge",
                    duration: 0.5,
                    frequency: 1,
                  },
                ])
              );
            }}
          >
            ajouter une tâche
          </button> */}
        </section>
      </div>
    </nav>
  );
}

export default App;

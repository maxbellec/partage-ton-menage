import React from 'react';

export default class Task extends React.Component {
  render() {
    return <div className="card" style={{'width': '220px'}}>
      <div className="card-content" style={{padding: '0.8rem'}}>
        <div>
          <p className={"subtitle is-5"}>{this.props.name}</p>
        </div>
        <div style={{display: 'flex', 'justify-content': 'space-between'}}>
          <p className="discreet" style={{'padding-right': '5px'}}>
            {this.props.duration}m
          </p>
          <p className="discreet" style={{'padding-left': '5px', 'text-align': 'right'}}>
            ttes les {this.props.frequency} sem
          </p>
        </div>
      </div>
      <footer className="card-footer">
        <a href="#" className="card-footer-item">Éditer</a>
        <a href="#" className="card-footer-item">Fait</a>
      </footer>
    </div>;
  }
}
